# oksconfig

## tdaq-11-02-00

Add `version` parameter to db spec as discussed in [ADTCC-328](https://its.cern.ch/jira/browse/ADTCC-328).
As an example, this spec "oksconfig:combined/partitions/ATLAS.data.xml&version=tag:r454833@ATLAS" defines ATLAS oks configuration for run 454833.